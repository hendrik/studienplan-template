Template für die Erstellung von Studienplänen. Erstellt auf der ZaPF 2014
in Düsseldorf für den AK Kommentierte Studienordnung.

Das aktuelle Ergebniss mit Daten einiger Universitäten findet sich [im
ZaPF-Wiki](https://vmp.ethz.ch/zapfwiki/index.php/Datei:KommentierteStudienordnungen.pdf)
